<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    /**
     * Get the Subject for the Question.
     */
    public function subjects()
    {
        return $this->belongsTo(Subjects::class);
    }

    /**
     * Get the Answers for the Question.
     */
    public function answers()
    {
        return $this->hasMany(Answers::class);
    }
}
