<?php

namespace App\Http\Controllers;

use App\Questions;
use App\Answers;
use App\Subjects;
use Dotenv\Regex\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Console\Question\Question;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('multiauth::admin.questions');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, [
            'subject' => 'required',
            'question' => 'required_without:questionImage',
            'questionImage' => 'required_without:question|mimes:jpeg,jpg,png',
            'answerOne' => 'required_without:answerOneImage',
            'answerOneImage' => 'required_without:answerOne|mimes:jpeg,jpg,png',
            'answerTwo' => 'required_without:answerTwoImage',
            'answerTwoImage' => 'required_without:answerTwo|mimes:jpeg,jpg,png',
            'answerThree' => 'required_without:answerThreeImage',
            'answerThreeImage' => 'required_without:answerThree|mimes:jpeg,jpg,png',
            'answerFour' => 'required_without:answerFourImage',
            'answerFourImage' => 'required_without:answerFour|mimes:jpeg,jpg,png',
            'correctAnswer' => 'required',
        ]);

        $questionImage = null;
        $answerOneImage = null;
        $answerTwoImage = null;
        $answerThreeImage = null;
        $answerFourImage = null;

        if ($request->hasFile('questionImage')) {
            $imageName = time() . '.' . request()->questionImage->getClientOriginalExtension();
            request()->questionImage->move(public_path('qimages'), $imageName);
            $questionImage = 'qimages/' . $imageName;
        }
        if ($request->hasFile('answerOneImage')) {
            $imageName = time() . '.' . request()->answerOneImage->getClientOriginalExtension();
            request()->answerOneImage->move(public_path('aimages'), $imageName);
            $answerOneImage = 'aimages/' . $imageName;
        }
        if ($request->hasFile('answerTwoImage')) {
            $imageName = time() . '.' . request()->answerTwoImage->getClientOriginalExtension();
            request()->answerTwoImage->move(public_path('aimages'), $imageName);
            $answerTwoImage = 'aimages/' . $imageName;
        }
        if ($request->hasFile('answerThreeImage')) {
            $imageName = time() . '.' . request()->answerThreeImage->getClientOriginalExtension();
            request()->answerThreeImage->move(public_path('aimages'), $imageName);
            $answerThreeImage = 'aimages/' . $imageName;
        }
        if ($request->hasFile('answerFourImage')) {
            $imageName = time() . '.' . request()->answerFourImage->getClientOriginalExtension();
            request()->answerFourImage->move(public_path('aimages'), $imageName);
            $answerFourImage = 'aimages/' . $imageName;
        }

        $question = new Questions();
        $question->subjects_id = $request['subject'];
        $question->question = $request['question'];
        $question->image_url = $questionImage;
        $question->save();

        // for($x=0; $x < 3; $x++) {
        // }
        $answers1 = new Answers();
        $answers1->answer = $request['answerOne'];
        $answers1->image_url = $answerOneImage;
        $answers1->correct = ($request['correctAnswer'] == 1 ? 1 : 0);

        $answers2 = new Answers();
        $answers2->answer = $request['answerTwo'];
        $answers2->image_url = $answerTwoImage;
        $answers2->correct = ($request['correctAnswer'] == 2 ? 1 : 0);

        $answers3 = new Answers();
        $answers3->answer = $request['answerThree'];
        $answers3->image_url = $answerThreeImage;
        $answers3->correct = ($request['correctAnswer'] == 3 ? 1 : 0);

        $answers4 = new Answers();
        $answers4->answer = $request['answerFour'];
        $answers4->image_url = $answerFourImage;
        $answers4->correct = ($request['correctAnswer'] == 4 ? 1 : 0);

        $question = $question->answers()->saveMany([$answers1, $answers2, $answers3, $answers4]);
        return Redirect::to(route('admin.questions.list'))->with('message', 'Model added Successfully');
        return 'Success';

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // $questions = Questions::find(1);
        // return dd($questions->subjects());
        $questions = Questions::all();
        return View('multiauth::admin.questionsList')->with('questions', $questions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function edit(Questions $questions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questions $questions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questions $questions)
    {
        //
    }

    public function generate()
    {
        $subjects = Subjects::all();
        // $questions = Questions::all();
        // return $subjects->questions();
        $data = [];
        foreach($subjects as $list) {
            $questions = [];
            foreach($list->questions as $qlist) {
                array_push($questions, $qlist->id);
            }
            shuffle($questions);
            array_push($data, ['subject' => $list['name'], ['questions' => $questions]]);
        }
        // return json_encode($data[2]);
    }
}
