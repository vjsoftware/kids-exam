<?php

namespace App\Http\Controllers;

use App\Answers;
use App\Exam;
use App\Subjects;
use App\Questions;
use Illuminate\Http\Request;

use Auth;

class ExamController extends Controller
{
    /**
     * Generate Question paper and start Exam.
     *
     * @return \Illuminate\Http\Response
     */
    public function start()
    {
        // Check if there's any pending Exam
        $userId = Auth::user()->id;
        $exam = Exam::where('user_id', $userId)->where('status', 0)->get();
        if(count($exam)) {
            $examId = $exam[0]->id;
            $json = json_decode($exam[0]->paper);
            $keys = array_keys($json[0]->questions);
            for ($x=0; $x < count($json); $x++) {
                for ($i = 0; $i < count($json[$x]->questions); $i++) {
                    $subject =  $json[$x]->subject;
                    foreach ($json[$x]->questions[$keys[$i]] as $key => $value) {
                        // echo $key . " : " . $value . "<br>";
                        if ($value == "0") {
                            $data = [
                                'examID' => $examId,
                                'subject' => $subject,
                                'question' => $key,
                            ];
                            // return redirect("/exam/$examId/$subject/$key");
                            return view('subject-instructions')->with('data', $data);
                                // ->header("Refresh", "2;url=/exam/$examId/$subject/$key");
                            return $key . " : " . $value;
                        }
                    }
                }    
            }
            
        } else {
            // return 'op';
            $subjects = Subjects::all();
            // $questions = Questions::all();
            // return $subjects->questions();
            $data = [];
            foreach ($subjects as $list) {
                $questions = [];
                foreach ($list->questions as $qlist) {
                    $qst = [
                        $qlist->id => 0
                    ];
                    array_push($questions, $qst);
                }
                shuffle($questions);
                array_push(
                    $data,
                    [
                        'subject' => $list['id'],
                        'questions' => $questions
                    ]
                );
            }
            $json = json_encode($data);
            $newExam = new Exam();
            $newExam->user_id = $userId;
            $newExam->paper = $json;
            $newExam->result = $json;
            $newExam->status = 0;
            $newExam->save();
            return redirect('/start');
        }
        
        
    }

    public function exam(Exam $exam, Subjects $subject, Questions $question)
    {
        // return $exam['paper'];
        // return $question['question'];
        
        // return $question->answers;
        // return $question->subjects['name'];
        $data = [
            'examID' => $exam,
            'subject' => $subject,
            'question' => $question
        ];
        return view('question')->with('data', $data);
    }

    public function answer(Request $request)
    {
        // return $request;
        $answerCheck = Answers::find($request['correctAnswer']);
        $userId = Auth::user()->id;
        $exam = Exam::find($request['examID']);
        // return $exam;
            $examId = $exam['id'];
            $json = json_decode($exam['paper']);
            $jsonResult = json_decode($exam['result']);
            $keys = array_keys($json[0]->questions);
            for ($x = 0; $x < count($json); $x++) {
                for ($i = 0; $i < count($json[$x]->questions); $i++) {
                    $subject =  $json[$x]->subject;
                    foreach ($json[$x]->questions[$keys[$i]] as $key => $value) {
                        // return json_encode($json[$x]->questions[$keys[$i]]);
                        echo $key . " : " . $value . "<br>";
                        if ($key == $request['question']) {
                            $json[$x]->questions[$keys[$i]] = [$request['question'] => 1];
                            $exam->paper = json_encode($json);
                            $exam->save();
                            if($answerCheck['correct'] == 1) {
                            if ($key == $request['question']) {
                                $jsonResult[$x]->questions[$keys[$i]] = [$request['question'] => 1];
                                $examt = Exam::find($request['examID']);
                                $examt->result = json_encode($jsonResult);
                                $examt->save();
                            }
                            
                                return response()->view('correct-answer')
                                    ->header("Refresh", "5;url=/next-question");
                            } else {
                                if($key == $request['question']) {
                                    $jsonResult[$x]->questions[$keys[$i]] = [$request['question'] => 0];
                                    $examty = Exam::find($request['examID']);
                                    $examty->result = json_encode($jsonResult);
                                    $examty->save();
                                }
                            
                                return response()->view('wrong-answer')
                                    ->header("Refresh", "5;url=/next-question");
                            }
                            // return $key . " : " . $value;
                            // return redirect("/exam/$examId/$subject/$key");
                            // return view('subject-instructions')->with('data', $data);
                            // ->header("Refresh", "2;url=/exam/$examId/$subject/$key");
                        }
                    }
                }
            }
        
        // return $answerCheck;
        // return 'answer';
    }

    public function nextQuestion()
    {
        // Check if there's any pending Exam
        $userId = Auth::user()->id;
        $exam = Exam::where('user_id', $userId)->where('status', 0)->get();
        if (count($exam)) {
            $examId = $exam[0]->id;
            $jsonResult = json_decode($exam[0]->result);
            $json = json_decode($exam[0]->paper);
            $keys = array_keys($json[0]->questions);
            for ($x = 0; $x < count($json); $x++) {
                for ($i = 0; $i < count($json[$x]->questions); $i++) {
                    $subject =  $json[$x]->subject;
                    foreach ($json[$x]->questions[$keys[$i]] as $key => $value) {
                        // echo $key . " : " . $value . "<br>";
                        if ($value == "0") {
                            // return redirect("/exam/$examId/$subject/$key");
                            return redirect("/exam/$examId/$subject/$key");
                        }
                    }
                }
            }
            $updateExam = Exam::find($examId);
            $updateExam->status = 1;
            $updateExam->save();
            return view('result')->with('data', $examId);
        } else {
            return redirect('/start');
        }
    }

    public function correctAnswer()
    {
        return view('success');
    }

    public function wrongAnswer()
    {
        return view('wrong-answer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        //
    }
}
