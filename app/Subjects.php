<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    /**
     * Get the Questions for the Subject.
     */
    public function questions()
    {
        return $this->hasMany(Questions::class);
    }
}
