<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{

    /**
     * Get the Question that owns the Answer.
     */
    public function questions()
    {
        return $this->belongsTo(Questions::class);
    }
}
