@php
    $pageName = 'Dashboard';
    $pageNameSub = '';
    use App\Exam;
    use App\Subjects;
@endphp
<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"><!-- /Added by HTTrack -->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<title>HANON | Exam - Result</title>
    <meta content="" name="description">
    @include('layouts/head')
</head>
<body>
	 <!-- Preloader Start Here -->
     @include('/layouts/preloader') 
     <!-- Dashboard Page Start Here -->
    <div id="wrapper" class="wrapper bg-ash">
        <!-- Header Menu Area Start Here -->
        @include('layouts/navbar')
        <!-- Header Menu Area End Here -->
        <!-- Page Area Start Here -->
        <div class="dashboard-page-one">
            <!-- Sidebar Area Start Here -->
           @include('layouts/sidebar')
            <!-- Sidebar Area End Here -->
            <div class="dashboard-content-one">
                <!-- Breadcubs Area Start Here -->
                <div class="breadcrumbs-area">
                    <h3>Employee Dashboard</h3>
                    <ul>
                        <li>
                            <a href="#">Result</a>
                        </li>
                    </ul>
                </div>
                <!-- Breadcubs Area End Here -->
                <div class="row">
                    <!-- Dashboard summery Start Here -->
                    <div class="col-lg-6 col-xl-6 col-4-xxxl">
                        <div class="card dashboard-card-five pd-b-20">
                            <div class="card-body pd-b-14">
                                <div class="heading-layout1">
                                    <div class="item-title">
                                        <h3>Result</h3>
                                    </div>
                                </div>
                                <div class="traffic-table table-responsive">
                                    <table class="table">
                                        <tbody>
                                            @php
                                        // echo $data;
                                            $exam = Exam::find($data);
                                            // dd($exam);
                                            if ($exam->count()) {
                                                $examId = $exam['id'];
                                                $json = json_decode($exam['result']);
                                                $keys = array_keys($json[0]->questions);
                                                $totcal = 0;
                                                for ($x = 0; $x < count($json); $x++) {
                                                    // $subjectName = 'subject'. $json[$x]->subject;
                                                    // echo $subjectName;
                                                    $cal = 0;
                                                    for ($i = 0; $i < count($json[$x]->questions); $i++) {
                                                        // echo $i;
                                                        $subject =  $json[$x]->subject;
                                                        foreach ($json[$x]->questions[$keys[$i]] as $key => $value) {
                                                            // echo $key . " : " . $value . "<br>";
                                                            if($value == 1) {
                                                                $cal++;
                                                                $totcal++;
                                                            }
                                                        }
                                                    }
                                                    $subject = Subjects::find($json[$x]->subject);
                                                    @endphp
                                                    <tr>
                                                        <td class="t-title pseudo-bg-Aquamarine">{{ $subject['name'] }}</td>
                                                        <td>{{count($json[$x]->questions)}}/{{ $cal }}</td>
                                                        </tr>
                                                    @php
                                                }
                                            }
                                        @endphp
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Dashboard summery End Here -->
                </div>
                <!-- Student Table Area Start Here -->
                
                <!-- Student Table Area End Here -->
                {{-- Footer Starts Here --}}
                @include('/layouts/footer')
                
            </div>
        </div>
        <!-- Page Area End Here -->
    </div>

     
     {{-- Scripts --}}
	@include('/layouts/scripts')
</body>
</html>