@php
    $pageName = 'Dashboard';
    $pageNameSub = '';
@endphp
<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"><!-- /Added by HTTrack -->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<title>HANON | Exam</title>
    <meta content="" name="description">
    @include('layouts/head')
</head>
<body>
	 <!-- Preloader Start Here -->
     @include('/layouts/preloader') 
     <!-- Dashboard Page Start Here -->
    <div id="wrapper" class="wrapper bg-ash">
        <!-- Header Menu Area Start Here -->
        @include('layouts/navbar')
        <!-- Header Menu Area End Here -->
        <!-- Page Area Start Here -->
        <div class="dashboard-page-one">
            <!-- Sidebar Area Start Here -->
           @include('layouts/sidebar')
            <!-- Sidebar Area End Here -->
            <div class="dashboard-content-one">
                <!-- Breadcubs Area Start Here -->
                <div class="breadcrumbs-area">
                    <h3>Employee Dashboard</h3>
                    <ul>
                        <li>
                            <a href="{{ route('home') }}">Home</a>
                        </li>
                    </ul>
                </div>
                <!-- Breadcubs Area End Here -->
                <div class="row">
                    <!-- Dashboard summery Start Here -->
                    <div class="col-12 col-12-xxxl">
                        <div class="row">
                            <div class="col-6-xxxl col-lg-3 col-sm-6 col-12">
                                <div class="dashboard-summery-two">
                                    <div class="item-content">
                                        <div class="item-number">Welcome to HANON Exam's</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Dashboard summery End Here -->
                </div>
                <!-- Student Table Area Start Here -->
                
                <!-- Student Table Area End Here -->
                {{-- Footer Starts Here --}}
                @include('/layouts/footer')
                
            </div>
        </div>
        <!-- Page Area End Here -->
    </div>

     
     {{-- Scripts --}}
	@include('/layouts/scripts')
</body>
</html>