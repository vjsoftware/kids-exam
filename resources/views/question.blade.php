@php
    $pageName = 'Questions';
    $pageNameSub = 'QuestionsAdd';

    use App\Subjects;

    $question = $data['question'];
    $examID = $data['examID'];
    $subject = $data['subject'];
@endphp
<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"><!-- /Added by HTTrack -->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<title>HANON | Login</title>
    <meta content="" name="description">
    @include('layouts/head')
</head>
<body>
	 <!-- Preloader Start Here -->
     @include('/layouts/preloader') 
     <!-- Dashboard Page Start Here -->
    <div id="wrapper" class="wrapper bg-ash">
        <!-- Header Menu Area Start Here -->
        @include('layouts/navbar')
        <!-- Header Menu Area End Here -->
        <!-- Page Area Start Here -->
        <div class="dashboard-page-one">
            <!-- Sidebar Area Start Here -->
           @include('layouts/sidebar')
            <!-- Sidebar Area End Here -->
            <div class="dashboard-content-one">
                <!-- Breadcubs Area Start Here -->
                <div class="breadcrumbs-area">
                    <h3>Subject: {{ $question->subjects['name'] }}</h3>
                </div>
                <!-- Breadcubs Area End Here -->
                <!-- All Subjects Area Start Here -->
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                @if ($question['question'] != null)
                                    <h3 style="text-decoration: underline; font-weight: bold;">{{ $question['question'] }}</h3>                                    
                                @else
                                    <img src="{{ $question['image_url'] }}" alt="image">
                                @endif
                            </div>
                        </div>
                        <form class="new-added-form" action="{{ route('answer') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="examID" value="{{ $examID['id'] }}">
                            <input type="hidden" name="subject" value="{{ $subject['id'] }}">
                            <input type="hidden" name="question" value="{{ $question['id'] }}">
                            <hr>
                            @foreach ($question->answers as $answers)
                                <div class="row">
                                        <div class="col-xl-3 col-lg-6 col-4">
                                            @if ($answers['answer'] != null)
                                                <label>{{ $answers['answer'] }}</label>
                                            @endif
                                        </div>
                                        <div class="col-lg-3 col-4">
                                            {{-- <label class="text-dark-medium">Answer 2 Image (300px X 300px)</label> --}}
                                            @if ($answers['image_url'] != null)
                                                <img src="{{ $answers['image_url'] }}" alt="image">
                                            @endif
                                        </div>
                                        <div class="col-xl-3 col-lg-6 col-4">
                                            <label style="">Correct <input type="radio" name="correctAnswer" value="{{ $answers['id'] }}" class=""></label>
                                        </div>
                                        
                                        
                                    </div>
                                    <hr>
                                @endforeach
                                
                                
                            
                            
                            @error('correctAnswer')
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                    <strong>{{ $message }}*</strong>
                                </span>
                            @enderror
                            <div class="col-12 form-group mg-t-8">
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Next</button>
                                {{-- <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button> --}}
                            </div>
                            
                        </form>
                    </div>
                </div>
                <!-- All Subjects Area End Here -->
                @include('/layouts/footer')            
            </div>
        </div>
        <!-- Page Area End Here -->
    </div>

     
     {{-- Scripts --}}
	@include('/layouts/scripts')
</body>
</html>