<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color" style="min-height: 93vh;">
               <div class="mobile-sidebar-header d-md-none">
                    <div class="header-logo">
                        <a href="index.html"><img src="{{ asset('') }}/img/logo1.png" alt="logo"></a>
                    </div>
               </div>
                <div class="sidebar-menu-content">
                    <ul class="nav nav-sidebar-menu sidebar-toggle-view">
                        <li class="nav-item">
                            <a href="{{ route('admin.home') }}" class="nav-link @if($pageName == 'Dashboard')menu-active @endif">
                                <i class="flaticon-open-book"></i>
                                <span class="@if($pageName == 'Dashboard')menu-active @endif">Dashboard</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.subjects') }}" class="nav-link @if($pageName == 'Subject')menu-active @endif">
                                <i class="flaticon-open-book"></i>
                                <span class="@if($pageName == 'Subject')menu-active @endif">Subject</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.employees') }}" class="nav-link @if($pageName == 'Employees')menu-active @endif">
                                <i class="flaticon-open-book"></i>
                                <span class="@if($pageName == 'Employees')menu-active @endif">Employee's</span></a>
                        </li>
                        <li class="nav-item sidebar-nav-item">
                            <a href="#" class="nav-link"><i class="flaticon-shopping-list"></i><span>Questions</span></a>
                            <ul class="nav sub-group-menu @if($pageName == 'Questions')sub-group-active @endif">
                                <li class="nav-item">
                                    <a href="{{ route('admin.questions.add') }}" class="nav-link @if($pageNameSub == 'QuestionsAdd')menu-active @endif"><i class="fas fa-angle-right"></i>Add</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.questions.list') }}" class="nav-link @if($pageNameSub == 'QuestionsAll')menu-active @endif"><i class="fas fa-angle-right"></i>All Questions</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item sidebar-nav-item">
                            <a href="#" class="nav-link"><i class="flaticon-shopping-list"></i><span>Employee's</span></a>
                            <ul class="nav sub-group-menu @if($pageName == 'Employees')sub-group-active @endif">
                                <li class="nav-item">
                                    <a href="{{ route('admin.questions.add') }}" class="nav-link @if($pageNameSub == 'EmployeesAdd')menu-active @endif"><i class="fas fa-angle-right"></i>Add</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.questions.list') }}" class="nav-link @if($pageNameSub == 'EmployeesAll')menu-active @endif"><i class="fas fa-angle-right"></i>Employee's List</a>
                                </li>
                            </ul>
                        </li>
                        
                        
                    </ul>
                </div>
            </div>