    <!-- jquery-->
    <script src="{{ asset('') }}/js/jquery-3.3.1.min.js"> </script>
    
    <!-- Plugins js -->
    <script src="{{ asset('') }}/js/plugins.js"> </script>
    
    <!-- Popper js -->
    <script src="{{ asset('') }}/js/popper.min.js"> </script> 
    
    <!-- Bootstrap js -->
    <script src="{{ asset('') }}/js/bootstrap.min.js"> </script> 

    <!-- Select 2 Js -->
    <script src="{{ asset('') }}/js/select2.min.js"></script>

    <!-- Counterup Js -->
    <script src="{{ asset('') }}/js/jquery.counterup.min.js"></script>

    <!-- Waypoints Js -->
    <script src="{{ asset('') }}/js/jquery.waypoints.min.js"></script>
    
    <!-- Scroll Up Js -->	 
    <script src="{{ asset('') }}/js/jquery.scrollUp.min.js"> </script> 

    <!-- Data Table Js -->
    <script src="{{ asset('') }}/js/jquery.dataTables.min.js"></script>

    <!-- Chart Js -->
    <script src="{{ asset('') }}/js/Chart.min.js"></script>
    
    <!-- Custom Js -->
	<script src="{{ asset('') }}/js/main.js"> </script>