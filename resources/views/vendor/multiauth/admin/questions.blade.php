@php
                            if ($errors->all() != '') {
                                # code...
                                // dd(print_r($errors));
                            }
                            @endphp
@php
    $pageName = 'Questions';
    $pageNameSub = 'QuestionsAdd';

    use App\Subjects;
@endphp
<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"><!-- /Added by HTTrack -->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<title>HANON | Login</title>
    <meta content="" name="description">
    @include('multiauth::layouts/head')
</head>
<body>
	 <!-- Preloader Start Here -->
     @include('multiauth::/layouts/preloader') 
     <!-- Dashboard Page Start Here -->
    <div id="wrapper" class="wrapper bg-ash">
        <!-- Header Menu Area Start Here -->
        @include('multiauth::layouts/navbar')
        <!-- Header Menu Area End Here -->
        <!-- Page Area Start Here -->
        <div class="dashboard-page-one">
            <!-- Sidebar Area Start Here -->
           @include('multiauth::layouts/sidebar')
            <!-- Sidebar Area End Here -->
            <div class="dashboard-content-one">
                <!-- Breadcubs Area Start Here -->
                <div class="breadcrumbs-area">
                    <h3>Add new Question</h3>
                </div>
                <!-- Breadcubs Area End Here -->
                <!-- All Subjects Area Start Here -->
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Add New Students</h3>
                            </div>
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                                    aria-expanded="false">...</a>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#"><i
                                            class="fas fa-times text-orange-red"></i>Close</a>
                                    <a class="dropdown-item" href="#"><i
                                            class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                    <a class="dropdown-item" href="#"><i
                                            class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                </div>
                            </div>
                        </div>
                        <form class="new-added-form" action="{{ route('admin.questions.create') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Subject *</label>
                                        @php
                                            $subjects = Subjects::all();
                                        @endphp
                                    <select class="select2" required autofocus name="subject">
                                        <option value="">Please Select Subject *</option>
                                        @foreach ($subjects as $list)
                                            <option value="{{ $list['id'] }}" @if(old('subject') == $list['id']) selected @endif>{{ $list['name'] }}</option>
                                        @endforeach
                                    </select>
                                    @error('subject')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-12 form-group">
                                    <label>Question *</label>
                                    <textarea name="question" id="question" class="textarea form-control" rows="9">{{ old('question') }}</textarea>
                                    @error('question')
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="col-lg-6 col-12 form-group mg-t-30">
                                    <label class="text-dark-medium">Question Image (300px X 300px)</label>
                                    <input type="file" name="questionImage" class="form-control-file">
                                    @error('questionImage')
                                    <span class="invalid-feedback" role="alert" style="display: block;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>

                                
                                
                            </div>
                            <hr>
                            <br>
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Answer 1 *</label>
                                    <input type="text" name="answerOne" placeholder="" class="form-control" value="{{ old('answerOne') }}">
                                    @error('answerOne')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-12 form-group mg-t-30">
                                    <label class="text-dark-medium">Answer 1 Image (300px X 300px)</label>
                                    <input type="file" name="answerImageOne" class="form-control-file">
                                    @error('answerImageOne')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Correct Answer *</label>
                                    <input type="radio" name="correctAnswer" value="1" class="form-control" @if(old('correctAnswer') == 1) checked @endif>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Answer 2 *</label>
                                    <input type="text" name="answerTwo" placeholder="" class="form-control" value="{{ old('answerTwo') }}">
                                    @error('answerTwo')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-12 form-group mg-t-30">
                                    <label class="text-dark-medium">Answer 2 Image (300px X 300px)</label>
                                    <input type="file" name="answerImageTwo" class="form-control-file">
                                    @error('answerImageTwo')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Correct Answer *</label>
                                    <input type="radio" name="correctAnswer" value="2" class="form-control" @if(old('correctAnswer') == 2) checked @endif>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Answer 3 *</label>
                                    <input type="text" name="answerThree" placeholder="" class="form-control" value="{{ old('answerThree') }}">
                                    @error('answerThree')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-12 form-group mg-t-30">
                                    <label class="text-dark-medium">Answer 3 Image (300px X 300px)</label>
                                    <input type="file" name="answerImageThree" class="form-control-file">
                                    @error('answerImageThree')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Correct Answer *</label>
                                    <input type="radio" name="correctAnswer" value="3" class="form-control" @if(old('correctAnswer') == 3) checked @endif>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Answer 4 *</label>
                                    <input type="text" name="answerFour" placeholder="" class="form-control" value="{{ old('answerFour') }}">
                                    @error('answerFour')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-lg-3 col-12 form-group mg-t-30">
                                    <label class="text-dark-medium">Answer 4 Image (300px X 300px)</label>
                                    <input type="file" name="answerImageFour" class="form-control-file">
                                    @error('answerImageFour')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                    <label>Correct Answer *</label>
                                    <input type="radio" name="correctAnswer" value="4" class="form-control" @if(old('correctAnswer') == 4) checked @endif>
                                </div>
                            </div>
                            <hr>
                            
                            @error('correctAnswer')
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                    <strong>{{ $message }}*</strong>
                                </span>
                            @enderror
                            <div class="col-12 form-group mg-t-8">
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                {{-- <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button> --}}
                            </div>
                            
                        </form>
                    </div>
                </div>
                <!-- All Subjects Area End Here -->
                @include('multiauth::/layouts/footer')            
            </div>
        </div>
        <!-- Page Area End Here -->
    </div>

     
     {{-- Scripts --}}
	@include('multiauth::/layouts/scripts')
</body>
</html>