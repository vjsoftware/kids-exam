@php
    $pageName = 'Subject';
    $pageNameSub = '';
@endphp
<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"><!-- /Added by HTTrack -->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<title>HANON | Login</title>
    <meta content="" name="description">
    @include('multiauth::layouts/head')
</head>
<body>
	 <!-- Preloader Start Here -->
     @include('multiauth::/layouts/preloader') 
     <!-- Dashboard Page Start Here -->
    <div id="wrapper" class="wrapper bg-ash">
        <!-- Header Menu Area Start Here -->
        @include('multiauth::layouts/navbar')
        <!-- Header Menu Area End Here -->
        <!-- Page Area Start Here -->
        <div class="dashboard-page-one">
            <!-- Sidebar Area Start Here -->
           @include('multiauth::layouts/sidebar')
            <!-- Sidebar Area End Here -->
            <div class="dashboard-content-one">
                <!-- Breadcubs Area Start Here -->
                <div class="breadcrumbs-area">
                    <h3>All Subjects</h3>
                </div>
                <!-- Breadcubs Area End Here -->
                <!-- All Subjects Area Start Here -->
                <div class="row">
                    <div class="col-4-xxxl col-12">
                        <div class="card height-auto">
                            <div class="card-body">
                                <div class="heading-layout1">
                                    <div class="item-title">
                                        <h3>Add New Subject</h3>
                                    </div>
                                </div>
                                <form class="new-added-form" action="{{ route('admin.subjects.create') }}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                            <label>Subject Name *</label>
                                            <input type="text" placeholder="Subject Name" name="subject" class="form-control" required autofocus>
                                            @error('subject')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 form-group mg-t-8">
                                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                            {{-- <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button> --}}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-8-xxxl col-12">
                        <div class="card height-auto">
                            <div class="card-body">
                                <div class="heading-layout1">
                                    <div class="item-title">
                                        @if(Session::has('message'))
                                            <p class="alert alert-success">{{ Session::get('message') }}</p>
                                        @endif
                                        <h3>All Subjects</h3>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table display data-table text-nowrap">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Subject Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($subjects)
                                                @foreach ($subjects as $list)
                                                    <tr>
                                                        <td>{{ $list['id'] }}</td>
                                                        <td>{{ $list['name'] }}</td>
                                                    </tr>           
                                                @endforeach
                                            @else
                                            <tr>
                                                <td></td>
                                                <td>Accounting</td>
                                            </tr>                                                
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- All Subjects Area End Here -->
                @include('multiauth::/layouts/footer')            
            </div>
        </div>
        <!-- Page Area End Here -->
    </div>

     
     {{-- Scripts --}}
	@include('multiauth::/layouts/scripts')
</body>
</html>