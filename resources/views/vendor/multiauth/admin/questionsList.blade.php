@php
    $pageName = 'Questions';
    $pageNameSub = 'QuestionsAll';

    use App\Subjects;
@endphp
<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"><!-- /Added by HTTrack -->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<title>HANON | Login</title>
    <meta content="" name="description">
    @include('multiauth::layouts/head')
</head>
<body>
	 <!-- Preloader Start Here -->
     @include('multiauth::/layouts/preloader') 
     <!-- Dashboard Page Start Here -->
    <div id="wrapper" class="wrapper bg-ash">
        <!-- Header Menu Area Start Here -->
        @include('multiauth::layouts/navbar')
        <!-- Header Menu Area End Here -->
        <!-- Page Area Start Here -->
        <div class="dashboard-page-one">
            <!-- Sidebar Area Start Here -->
           @include('multiauth::layouts/sidebar')
            <!-- Sidebar Area End Here -->
            <div class="dashboard-content-one">
                <!-- Breadcubs Area Start Here -->
                <div class="breadcrumbs-area">
                    <h3>Question List</h3>
                </div>
                <!-- Breadcubs Area End Here -->
                <!-- All Subjects Area Start Here -->
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>All Questions</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table display data-table text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Subject</th>
                                        <th>Question</th>
                                        <th>Answers</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $appUrl = config('app.url');
                                    @endphp
                                    @if (count($questions))
                                    @foreach ($questions as $list)
                                        <tr>
                                            <td>{{ $list->subjects['name'] }}</td>
                                            <td>
                                                {{ ($list['question'] != null ? $list['question'] : "" ) }}
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                @if ($list['image_url'])
                                                    <img src="{{$appUrl.'/'.$list['image_url']}}" style="height: 80px;"/>
                                                @endif
                                            </td>
                                            <td>
                                                <div>{{ ($list->answers[0]['answer'] != null ? $list->answers[0]['answer'] : "" ) }}</div>
                                                <div>{{ ($list->answers[1]['answer'] != null ? $list->answers[1]['answer'] : "" ) }}</div>
                                                <div>{{ ($list->answers[2]['answer'] != null ? $list->answers[2]['answer'] : "" ) }}</div>
                                                <div>{{ ($list->answers[3]['answer'] != null ? $list->answers[3]['answer'] : "" ) }}</div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- All Subjects Area End Here -->
                @include('multiauth::/layouts/footer')            
            </div>
        </div>
        <!-- Page Area End Here -->
    </div>

     
     {{-- Scripts --}}
	@include('multiauth::/layouts/scripts')
</body>
</html>