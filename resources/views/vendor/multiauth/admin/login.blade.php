<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"><!-- /Added by HTTrack -->
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<title>HANON | Login</title>
    <meta content="" name="description">
    @include('multiauth::layouts/head')
</head>
<body>
	 <!-- Preloader Start Here -->
	 @include('multiauth::/layouts/preloader') <!-- Login Page Start Here -->
	<div class="login-page-wrap">
		<div class="login-page-content">
			<div class="login-box">
				<div class="item-logo"><img alt="logo" src="img/logo2.png"></div>
                <form action="{{ route('admin.login') }}" method="POST" class="login-form">
                    @csrf
					<div class="form-group">
                        <label>Email Address</label> 
                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter E@Mail Address" type="email" name="email" value="{{ old('email') }}" required autofocus> 
                        <i class="far fa-envelope"></i>
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                        </span> 
                        @endif
					</div>
					<div class="form-group">
                        <label>Password</label> 
                        <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Enter password" type="password"> 
                        <i class="fas fa-lock"></i>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                        </span> 
                        @endif
					</div>
					<div class="form-group d-flex align-items-center justify-content-between">
						<div class="form-check">
							<input class="form-check-input" id="remember-me" type="checkbox" name="remember" id="remember" {{ old( 'remember') ? 'checked' : '' }}> <label class="form-check-label" for="remember-me">Remember Me</label>
						</div>
					</div>
					<div class="form-group">
						<button class="login-btn" type="submit">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div><!-- Login Page End Here -->
	@include('multiauth::/layouts/scripts')
</body>
</html>