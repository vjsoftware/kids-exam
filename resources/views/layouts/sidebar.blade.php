<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color" style="min-height: 93vh;">
               <div class="mobile-sidebar-header d-md-none">
                    <div class="header-logo">
                        <a href="index.html"><img src="{{ asset('') }}/img/logo1.png" alt="logo"></a>
                    </div>
               </div>
                <div class="sidebar-menu-content">
                    <ul class="nav nav-sidebar-menu sidebar-toggle-view">
                        <li class="nav-item">
                            <a href="{{ route('home') }}" class="nav-link @if($pageName == 'Dashboard')menu-active @endif">
                                <i class="flaticon-open-book"></i>
                                <span class="@if($pageName == 'Dashboard')menu-active @endif">Dashboard</span></a>
                        </li>
                        
                        
                        
                    </ul>
                </div>
            </div>