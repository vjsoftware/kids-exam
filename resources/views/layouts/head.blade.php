    <meta content="width=device-width, initial-scale=1" name="viewport">
    
    <!-- Favicon -->
    <link href="{{ asset('') }}/img/favicon.png" rel="shortcut icon" type="image/x-icon">
    <!-- Normalize CSS -->
    <link href="{{ asset('') }}/css/normalize.css" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{ asset('') }}/css/main.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('') }}/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fontawesome CSS -->
    <link href="{{ asset('') }}/css/all.min.css" rel="stylesheet">
    <!-- Flaticon CSS -->
    <link href="{{ asset('') }}/fonts/flaticon.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="{{ asset('') }}/css/animate.min.css" rel="stylesheet">
    <!-- Full Calander CSS -->
    <link href="{{ asset('') }}/css/fullcalendar.min.css" rel="stylesheet">
    <!-- Select 2 CSS -->
    <link href="{{ asset('') }}/css/select2.min.css" rel="stylesheet">
    <!-- Datatable CSS -->
    <link href="{{ asset('') }}/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('') }}/style.css" rel="stylesheet">

    {{-- <style>
        html, body {
            height: 100%;
        }
    </style> --}}
    
    <!-- Modernize js -->
	<script src="{{ asset('') }}/js/modernizr-3.6.0.min.js"> </script>