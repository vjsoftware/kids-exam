<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/instructions', 'HomeController@instructions')->name('instructions');

// Exam Route's
Route::get('/start', 'ExamController@start')->name('start');
Route::get('/exam/{exam}/{subject}/{question}', 'ExamController@exam')->name('start');
Route::post('/answer-submit', 'ExamController@answer')->name('answer');
Route::get('/next-question', 'ExamController@nextQuestion')->name('nextQuestion');

// Answer Result Routes
Route::get('/correct-answer', 'ExamController@correctAnswer')->name('correct-answer');
Route::get('/wrong-answer', 'ExamController@wrongAnswer')->name('wrong-answer');
